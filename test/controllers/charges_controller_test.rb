require 'test_helper'

class ChargesControllerTest < ActionController::TestCase
  setup do
    @charge = charges(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:charges)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create charge" do
    assert_difference('Charge.count') do
      post :create, charge: { amount: @charge.amount, city: @charge.city, colonia: @charge.colonia, conekta_id: @charge.conekta_id, conekta_status: @charge.conekta_status, counter: @charge.counter, country: @charge.country, currency: @charge.currency, delivery_service: @charge.delivery_service, full_address: @charge.full_address, livemode: @charge.livemode, name: @charge.name, payment_bar_url: @charge.payment_bar_url, payment_barcode: @charge.payment_barcode, payment_type: @charge.payment_type, phone_number: @charge.phone_number, ship_number: @charge.ship_number, state: @charge.state, status: @charge.status, stock_discounted: @charge.stock_discounted, user_id: @charge.user_id, zip_code: @charge.zip_code }
    end

    assert_redirected_to charge_path(assigns(:charge))
  end

  test "should show charge" do
    get :show, id: @charge
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @charge
    assert_response :success
  end

  test "should update charge" do
    patch :update, id: @charge, charge: { amount: @charge.amount, city: @charge.city, colonia: @charge.colonia, conekta_id: @charge.conekta_id, conekta_status: @charge.conekta_status, counter: @charge.counter, country: @charge.country, currency: @charge.currency, delivery_service: @charge.delivery_service, full_address: @charge.full_address, livemode: @charge.livemode, name: @charge.name, payment_bar_url: @charge.payment_bar_url, payment_barcode: @charge.payment_barcode, payment_type: @charge.payment_type, phone_number: @charge.phone_number, ship_number: @charge.ship_number, state: @charge.state, status: @charge.status, stock_discounted: @charge.stock_discounted, user_id: @charge.user_id, zip_code: @charge.zip_code }
    assert_redirected_to charge_path(assigns(:charge))
  end

  test "should destroy charge" do
    assert_difference('Charge.count', -1) do
      delete :destroy, id: @charge
    end

    assert_redirected_to charges_path
  end
end
