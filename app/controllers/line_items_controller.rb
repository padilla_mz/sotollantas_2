class LineItemsController < ApplicationController
  include CurrentCart
  before_action :set_cart, only: [:create, :decrease, :destroy, :increase, :update_multiple]
  before_action :set_line_item, only: [:show, :edit, :update, :destroy]
  before_action :check_admin, only: [:index, :show, :new, :edit]


  # GET /line_items
  # GET /line_items.json
  def index
    @line_items = LineItem.all
  end

  # GET /line_items/1
  # GET /line_items/1.json
  def show
  end

  # GET /line_items/new
  def new
    @line_item = LineItem.new
  end

  # GET /line_items/1/edit
  def edit
  end

  # POST /line_items
  # POST /line_items.json
  def create
    # @line_item = LineItem.new(line_item_params)

    product = Product.find(params[:product_id])
    quantity = params[:line_item][:quantity]
    weight = product.weight

    @line_item = @cart.add_product(product.id, product.price, quantity, weight)


    respond_to do |format|
      if @line_item.save
        format.html { redirect_to show_product_path(product), notice: 'Producto añadido al carrito...' }
        # format.json { render :show, status: :created, location: @line_item }
        format.js { @current_item = @line_item }

      else
        format.html { render :new }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /line_items/1
  # PATCH/PUT /line_items/1.json
  def update
    respond_to do |format|
      if @line_item.update(line_item_params)
        format.html { redirect_to root_path, notice: 'Line item was successfully updated.' }
        format.json { render :show, status: :ok, location: @line_item }
      else
        format.html { render :edit }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  #POST 
  # def edit_multiple
  #   @line_items = LineItem.find(params[:product_ids])

  # end

  #PUT
  def update_multiple
    @product_ids = params[:product_ids]
    @quantities = params[:product_quantities]

    @product_ids.zip(@quantities).each do |product_id, quantity|
      @line_item = @cart.update_product_quantity(product_id, quantity)
      @line_item.save
    end


    respond_to do |f|
      if @line_item.save
        f.html {redirect_to cart_resume_path, notice: 'productos actualizados!!'}
      else
        f.html {redirect_to cart_resume_path, notice: 'no paso nada loco :('}
      end
    end
  end

  # DELETE /line_items/1
  # DELETE /line_items/1.json
  def destroy
    @line_item.destroy
    respond_to do |format|
      format.html { redirect_to cart_resume_path, notice: 'Producto removido!' }
      # format.json { head :no_content }
    end
  end

  def increase
    item = LineItem.find(params[:id])

    @line_item = @cart.add_pp(item)

    respond_to do |format|
      if @line_item.save
        format.html { redirect_to cart_resume_path, notice: 'itemmmmm updated!'}
        format.js
      else
        format.html { redirect_to root_path }
      end
    end
  end

  def decrease
    product = LineItem.find(params[:id])

    @line_item = @cart.decrease(product)

    respond_to do |format|
      if @line_item.save
        format.html { redirect_to cart_resume_path, notice: 'itemmmmm updated!'}
        format.js { @current_item = @line_item }
      else
        format.html { render action: "edit" }
      end
    end
  end

  private
    def check_admin
      unless current_user.is?(:super) or current_user.is?(:admin)
        redirect_to root_path, notice: "?"
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_line_item
      @line_item = LineItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def line_item_params
      params.require(:line_item).permit(:cart_id, :quantity, :price, :post_id, :charge_id, :property)
    end
end
