class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include CurrentCart
  protect_from_forgery with: :exception
  before_action :authenticate_user!, :set_cart, :set_stuff
  before_action :configure_permitted_parameters, if: :devise_controller?


  def set_stuff
    @pages = Page.where(published: true)
    
  end

  protected

  	def configure_permitted_parameters
  		devise_parameter_sanitizer.for(:sign_up) << :name
      # devise_parameter_sanitizer.for(:sign_up) << :twitter
      # devise_parameter_sanitizer.for(:sign_up) << :avatar
  		# devise_parameter_sanitizer.for(:sign_up) << :role
  		devise_parameter_sanitizer.for(:account_update) << :name
      # devise_parameter_sanitizer.for(:account_update) << :twitter
      # devise_parameter_sanitizer.for(:account_update) << :avatar
      # devise_parameter_sanitizer.for(:account_update) << :web_profile
  		# devise_parameter_sanitizer.for(:account_update) << :role
  	end
end
