class PagesController < ApplicationController
  before_action :set_page, only: [:show, :edit, :update, :destroy, :front_show]
  skip_before_action :authenticate_user!, only: [:front_show]
  before_action :check_admin, except: [:front_show]

  layout 'admin'

  # GET /pages
  # GET /pages.json
  def index
    @body_class = "admin"
    @pages = Page.all
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
    @body_class = "admin"

  end

  def front_show
    
  end

  # GET /pages/new
  def new
    @body_class = "admin"

    @page = Page.new
  end

  # GET /pages/1/edit
  def edit
    @body_class = "admin"

  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(page_params)

    respond_to do |format|
      if @page.save
        format.html { redirect_to @page, notice: 'Page was successfully created.' }
        format.json { render :show, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to @page, notice: 'Page was successfully updated.' }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url, notice: 'Page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def check_admin
      unless current_user.is?(:super) or current_user.is?(:admin)
        redirect_to root_path, notice: "?"
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Page.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(:title, :content, :published, :published_at)
    end
end
