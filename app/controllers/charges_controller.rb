class ChargesController < ApplicationController
  before_action :set_charge, only: [:show, :edit, :update, :destroy, :show_charge]
  layout "admin", only: [:index, :show, :edit]
  before_action :check_admin, only: [:index, :show, :new, :edit]


  # GET /charges
  # GET /charges.json
  def index
    @body_class = 'admin'

    @st = Charge.ransack(params[:q])
    @charges = @st.result(distinct: true).default.paginate(per_page: 30, page: params[:page])
  end

  # GET /charges/1
  # GET /charges/1.json
  def show
    @body_class = 'admin'

  end

  # GET /charges/new
  def new

    @charge = Charge.new
  end

  # GET /charges/1/edit
  def edit
    @body_class = 'admin'

  end

  # POST /charges
  # POST /charges.json

  def create
    
  end


  def create_3
    @body_class = "cart"
    @total_kg = @cart.line_items.sum(:weight).to_i
    @subtotal = @cart.total_price_cart
    @ship_price = calculate_shipping_amount @total_kg
    @total = ((@ship_price + @subtotal)* (0.079)) + (@ship_price + @subtotal)


    order_sku = unique_id(@total)

    
    begin
      charge = Conekta::Charge.create({
        amount: (@total * 100),
        currency: "MXN",
        description: "Compra sotollantas",
        reference_id: "#{order_sku}",
        card: params[:conektaTokenId],
        details: {
          name: params[:charge][:name],
          email: current_user.email,
          phone: params[:charge][:phone_number],
          billing_address: {
            company_name: "n/a",
            street1: params[:charge][:full_address],
            city: params[:charge][:city],
            state: params[:charge][:state],
            zip: params[:charge][:zip_code],
            phone: params[:charge][:phone_number],
            email: current_user.email
          }
        }
        #"tok_a4Ff0dD2xYZZq82d9"
      })
      # @charge = Charge.new(charge_params)
      @charge = Charge.new(
          user_id: current_user.id.to_i,
          name: params[:charge][:name],
          full_address: params[:charge][:full_address],
          colonia: params[:charge][:colonia],
          city: params[:charge][:city],
          country: "México",
          state: params[:charge][:state],
          zip_code: params[:charge][:zip_code],
          phone_number: params[:charge][:phone_number],
          livemode: charge.livemode,
          conekta_status: charge.status,
          amount: @total.to_i,
          conekta_id: order_sku,
          payment_type: "Tarjeta de credito",
          delivery_service: "FedEx",
          currency: "MXN"
        )
      @charge.add_line_items_from_cart(@cart)
      @charge.line_items.each do |line_item|
        line_item.product.decrement!(:stock, line_item.quantity)
      end
      @charge.stock_discounted = true
      # @charge.save

      # respond_to do |f|
      if @charge.save

        AdminMailer.charge_paid(@charge).deliver
        AdminMailer.charge_paid_admin(@charge).deliver
        Cart.destroy(session[:cart_id])
        session[:cart_id] = nil
        # respond_with(@charge, location: show_charges_path(@charge))
        # f.html {redirect_to show_charges_path(@charge), notice: "pago procesado!"}

        redirect_to show_charge_path(@charge)
      else
        # f.html {redirect_to root_path, notice: "algo salio mal!"}
        redirect_to root_path
      end  
      # end
    rescue Conekta::ParameterValidationError => e
      #alguno de los parametros fueron invalidos
      puts e.message
    rescue Conekta::ProcessingError => e 
      # la tarjeta no pudo ser procesada, por ejemplo tarjeta declinada
      puts e.message
      redirect_to credit_pay_path, alert: "Tarjeta no pudo ser procesada, la tarjeta fue declinada, intenta nuevamente o con otra tarjeta."
    rescue Conekta::Error => e
      #un error ocurrió que no sucede en el flujo normal de cobros como por ejemplo un auth_key incorrecto
      redirect_to credit_pay_path, alert: "Tarjeta no pudo ser procesada, intenta nuevamente."  
      puts e.message
    
    end

  end

  def create_2
    @body_class = "cart"
    @total_kg = @cart.line_items.sum(:weight).to_i
    @subtotal = @cart.total_price_cart
    @ship_price = calculate_shipping_amount @total_kg
    @total = ((@ship_price + @subtotal)* (0.049)) + (@ship_price + @subtotal)


    order_sku = unique_id(@total)

    
    begin
      charge = Conekta::Charge.create({
        amount: (@total * 100),
        currency: "MXN",
        description: "Compra sotollantas",
        reference_id: "#{order_sku}",
        card: params[:conektaTokenId],
        details: {
          name: params[:charge][:name],
          email: current_user.email,
          phone: params[:charge][:phone_number],
          billing_address: {
            company_name: "n/a",
            street1: params[:charge][:full_address],
            city: params[:charge][:city],
            state: params[:charge][:state],
            zip: params[:charge][:zip_code],
            phone: params[:charge][:phone_number],
            email: current_user.email
          }
        }
        #"tok_a4Ff0dD2xYZZq82d9"
      })
      # @charge = Charge.new(charge_params)
      @charge = Charge.new(
          user_id: current_user.id.to_i,
          name: params[:charge][:name],
          full_address: params[:charge][:full_address],
          colonia: params[:charge][:colonia],
          city: params[:charge][:city],
          country: "México",
          state: params[:charge][:state],
          zip_code: params[:charge][:zip_code],
          phone_number: params[:charge][:phone_number],
          livemode: charge.livemode,
          conekta_status: charge.status,
          amount: @total,
          conekta_id: order_sku,
          payment_type: "Tarjeta de credito",
          delivery_service: "FedEx",
          currency: "MXN"
        )
      @charge.add_line_items_from_cart(@cart)
      @charge.line_items.each do |line_item|
        line_item.product.decrement!(:stock, line_item.quantity)
      end
      @charge.stock_discounted = true
      # @charge.save

      # respond_to do |f|
      if @charge.save

        AdminMailer.charge_paid(@charge).deliver
        AdminMailer.charge_paid_admin(@charge).deliver
        Cart.destroy(session[:cart_id])
        session[:cart_id] = nil
        # respond_with(@charge, location: show_charges_path(@charge))
        # f.html {redirect_to show_charges_path(@charge), notice: "pago procesado!"}

        redirect_to show_charge_path(@charge)
      else
        # f.html {redirect_to root_path, notice: "algo salio mal!"}
        redirect_to root_path
      end  
      # end
    rescue Conekta::ParameterValidationError => e
      #alguno de los parametros fueron invalidos
      puts e.message
    rescue Conekta::ProcessingError => e 
      # la tarjeta no pudo ser procesada, por ejemplo tarjeta declinada
      puts e.message
      redirect_to credit_pay_path, alert: "Tarjeta no pudo ser procesada, la tarjeta fue declinada, intenta nuevamente o con otra tarjeta."
    rescue Conekta::Error => e
      #un error ocurrió que no sucede en el flujo normal de cobros como por ejemplo un auth_key incorrecto
      redirect_to credit_pay_path, alert: "Tarjeta no pudo ser procesada, intenta nuevamente."  
      puts e.message
    
    end

  end

  def create_1
    @body_class = "cart"
    @total_kg = @cart.line_items.sum(:weight).to_i
    @subtotal = @cart.total_price_cart
    @ship_price = calculate_shipping_amount @total_kg
    @total = @ship_price + @subtotal

    order_sku = unique_id(@total)

    
    begin
      charge = Conekta::Charge.create({
        amount: (@total * 100),
        currency: "MXN",
        description: "Compra sotollantas",
        reference_id: "#{order_sku}",
        card: params[:conektaTokenId],
        details: {
          name: params[:charge][:name],
          email: current_user.email,
          phone: params[:charge][:phone_number],
          billing_address: {
            company_name: "n/a",
            street1: params[:charge][:full_address],
            city: params[:charge][:city],
            state: params[:charge][:state],
            zip: params[:charge][:zip_code],
            phone: params[:charge][:phone_number],
            email: current_user.email
          }
        }
        #"tok_a4Ff0dD2xYZZq82d9"
      })
      # @charge = Charge.new(charge_params)
      @charge = Charge.new(
          user_id: current_user.id.to_i,
          name: params[:charge][:name],
          full_address: params[:charge][:full_address],
          colonia: params[:charge][:colonia],
          city: params[:charge][:city],
          country: "México",
          state: params[:charge][:state],
          zip_code: params[:charge][:zip_code],
          phone_number: params[:charge][:phone_number],
          livemode: charge.livemode,
          conekta_status: charge.status,
          amount: @total,
          conekta_id: order_sku,
          payment_type: "Tarjeta de credito",
          delivery_service: "FedEx",
          currency: "MXN"
        )
      @charge.add_line_items_from_cart(@cart)
      @charge.line_items.each do |line_item|
        line_item.product.decrement!(:stock, line_item.quantity)
      end
      @charge.stock_discounted = true
      # @charge.save

      # respond_to do |f|
      if @charge.save

        AdminMailer.charge_paid(@charge).deliver
        AdminMailer.charge_paid_admin(@charge).deliver
        Cart.destroy(session[:cart_id])
        session[:cart_id] = nil
        # respond_with(@charge, location: show_charges_path(@charge))
        # f.html {redirect_to show_charges_path(@charge), notice: "pago procesado!"}

        redirect_to show_charge_path(@charge)
      else
        # f.html {redirect_to root_path, notice: "algo salio mal!"}
        redirect_to root_path
      end  
      # end
    rescue Conekta::ParameterValidationError => e
      #alguno de los parametros fueron invalidos
      puts e.message
    rescue Conekta::ProcessingError => e 
      # la tarjeta no pudo ser procesada, por ejemplo tarjeta declinada
      puts e.message
      redirect_to credit_pay_path, alert: "Tarjeta no pudo ser procesada, la tarjeta fue declinada, intenta nuevamente o con otra tarjeta."
    rescue Conekta::Error => e
      #un error ocurrió que no sucede en el flujo normal de cobros como por ejemplo un auth_key incorrecto
      redirect_to credit_pay_path, alert: "Tarjeta no pudo ser procesada, intenta nuevamente."  
      puts e.message
    
    end

  end

  # PATCH/PUT /charges/1
  # PATCH/PUT /charges/1.json
  def update

    if (@charge.stock_discounted == false) and (params[:charge][:status] == "enviado")
      @charge.update(charge_params)

      @charge.line_items.each do |line_item|
        line_item.product.decrement!(:stock, line_item.quantity)
      end

      @charge.stock_discounted = true
      @charge.save
    else
      @charge.update(charge_params)
    end
    
    # respond_with(@charge)

    respond_to do |format|
      if @charge.update(charge_params)
        format.html { redirect_to edit_charge_path(@charge), notice: 'Cargo editado' }
        format.json { render :show, status: :ok, location: @charge }
      else
        format.html { render :edit }
        format.json { render json: @charge.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /charges/1
  # DELETE /charges/1.json
  def destroy
    @charge.destroy
    respond_to do |format|
      format.html { redirect_to charges_url, notice: 'Charge was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def cart_resume
    @line_item = LineItem.new
    @body_class = "cart"
    @total_kg = (@cart.line_items.sum(:weight).to_i)
    @subtotal = @cart.total_price_cart
    @ship_price = calculate_shipping_amount @total_kg
    @total = @ship_price + @subtotal
  end


  def credit
    @charge = Charge.new
    @body_class = "credit-card"
    @total_kg = @cart.line_items.sum(:weight).to_i
    @subtotal = @cart.total_price_cart
    @ship_price = calculate_shipping_amount @total_kg
    @total = @ship_price + @subtotal

    @option = params[:pay_option].to_i
    if @option == 1
      # charge directly
      redirect_to :credit_1
    elsif @option == 2
      # charge 3 months
      redirect_to :credit_2
    elsif @option == 3
      # charge 6 months
      redirect_to :credit_3
    end
  end

  def credit_1
    # direct pay
    @charge = Charge.new
    @body_class = "credit-card"
    @total_kg = @cart.line_items.sum(:weight).to_i
    @subtotal = @cart.total_price_cart
    @ship_price = calculate_shipping_amount @total_kg
    @total = @ship_price + @subtotal
  end
  def credit_2
    # +4.9%
    @charge = Charge.new
    @body_class = "credit-card"
    @total_kg = @cart.line_items.sum(:weight).to_i

    @subtotal = @cart.total_price_cart
    @ship_price = calculate_shipping_amount @total_kg
    @total = ((@ship_price + @subtotal)* (0.049)) + (@ship_price + @subtotal)
  end
  def credit_3
    # +7.9%

    @charge = Charge.new
    @body_class = "credit-card"

    @total_kg = @cart.line_items.sum(:weight).to_i
    @subtotal = @cart.total_price_cart
    @ship_price = calculate_shipping_amount @total_kg
    @total = ((@ship_price + @subtotal)* (0.079)) + (@ship_price + @subtotal)

  end

  def oxxo
    redirect_to root_path if @cart.line_items.empty?
    @charge = Charge.new
    @body_class = "credit-card"
    @total_kg = @cart.line_items.sum(:weight).to_i
    @subtotal = @cart.total_price_cart
    @ship_price = calculate_shipping_amount @total_kg
    @total = @ship_price + @subtotal

    if @total > 10000
      redirect_to cart_resume_path, notice: 'no puedes pagar mediante oxxo esta cantidad'
    end

  end

  def oxxo_pay

    @body_class = "credit-card"
    @total_kg = @cart.line_items.sum(:weight).to_i
    @subtotal = @cart.total_price_cart
    @ship_price = calculate_shipping_amount @total_kg
    @total = @ship_price + @subtotal


    order_sku = unique_id(@total)

    begin
      charge = Conekta::Charge.create({
        currency: "MXN",
        amount: (@total * 100),
        description: "Pago por medio de tiendas OXXO sotollantas",
        reference_id: "#{order_sku}",
        cash: {
          type: "oxxo"  
        },
        details: {
          name: params[:charge][:name],
          email: current_user.email,
          phone: params[:charge][:phone_number],
          billing_address: {
            company_name: "n/a",
            street1: params[:charge][:full_address],
            city: params[:charge][:city],
            state: params[:charge][:state],
            zip: params[:charge][:zip_code],
            phone: params[:charge][:phone_number],
            email: current_user.email
          }

        }
        #"tok_a4Ff0dD2xYZZq82d9"
      })
      
      
      @charge = Charge.new(
        user_id: current_user.id,
        name: params[:charge][:name],
        full_address: params[:charge][:full_address],
        colonia: params[:charge][:colonia],
        city: params[:charge][:city],
        country: "México",
        state: params[:charge][:state],
        zip_code: params[:charge][:zip_code],
        phone_number: params[:charge][:phone_number],
        livemode: charge.livemode,
        conekta_status: charge.status,
        amount: (@total.to_i),
        conekta_id: order_sku,
        payment_type: "OXXO",
        payment_barcode: charge.payment_method.barcode,
        payment_bar_url: charge.payment_method.barcode_url,
        delivery_service: "FedEx",
        currency: "MXN"
      )
      @charge.add_line_items_from_cart(@cart)
      # @charge.save
      
      if @charge.save
        AdminMailer.oxxo_created(@charge).deliver
        AdminMailer.oxxo_created_admin(@charge).deliver
        Cart.destroy(session[:cart_id])
        session[:cart_id] = nil
        redirect_to show_charge_path(@charge), notice: "cargo creado con éxito"

      else
        redirect_to oxxo_path, notice: "algo salio mal..."
      end
      
    rescue Conekta::ParameterValidationError => e
      #alguno de los parametros fueron invalidos
      puts e.message
    rescue Conekta::ProcessingError => e 
      # la tarjeta no pudo ser procesada
      puts e.message
    rescue Conekta::Error => e
      #un error ocurrió que no sucede en el flujo normal de cobros como por ejemplo un auth_key incorrecto
      redirect_to oxxo_charges_path, alert: "El cargo no pudo ser procesado"
      puts e.message
      
    end
  end

  def show_charge
    redirect_to root_path unless @charge.user.id == current_user.id
    @body_class = 'credit-card'
  end

  def calculate_shipping_amount(weight)
    # if weight > 0
    #   interval = 2.0
    # elsif weight > 50
    #   interval = 1.0
    # end

    ((weight/KG_PACKAGE).ceil) * SHIP_PRICE

    # return ((items/interval).ceil) * SHIP_PRICE
  end
  def send_number
    @charge = Charge.find(params[:charge_id])

    unless @charge.ship_number
      redirect_to edit_charge_path(@charge), notice: "No hay numero de guia" 
    else
      AdminMailer.shipped(@charge).deliver
      @charge.increment(:counter)
      @charge.save
      redirect_to edit_charge_path(@charge), notice: "Numero ha sido enviado!"
    end
  end

  private
    def check_admin
      unless current_user.is?(:super) or current_user.is?(:admin)
        redirect_to root_path, notice: "?"
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_charge
      @charge = Charge.find(params[:id])
    end
    def unique_id(y)
      time = Time.new
      "#{time.month}#{time.day}#{time.year}#{time.hour}#{time.min}#{current_user.name.parameterize}#{y}"
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def charge_params
      params.require(:charge).permit(:status, :ship_number, :name, :full_address, :colonia, :city, :country, :state, :zip_code, :phone_number, :conekta_id, :livemode, :conekta_status, :amount, :payment_type, :payment_barcode, :payment_bar_url, :user_id, :counter, :stock_discounted)
    end
end
