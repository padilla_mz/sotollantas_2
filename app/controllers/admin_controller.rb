class AdminController < ApplicationController
  before_action :set_user, only: [:show_user, :edit, :update, :destroy_user]
  before_action :check_admin
  layout "admin"


  def charts
    @body_class = 'admin'
  end

  # default users
  def n_users
  	@body_class = 'admin'
    @st = User.ransack(params[:q])
    @users = @st.result(distinct: true).normal_users.order(email: :asc).paginate(per_page: 100, page: params[:page])
  end

  # admin users
  def a_users
  	@body_class = 'admin'
    @st = User.ransack(params[:q])
    @users = @st.result(distinct: true).admins.order(name: :asc).paginate(per_page: 100, page: params[:page])
  end

  # GET admin show_user
  def show_user
  	@body_class = 'admin'
  end

  # GET admin user_edit
  def edit
    redirect_to root_path unless current_user.is?(:super)
  	@body_class = 'admin'
  end

  # PUT
  def update
    redirect_to root_path unless current_user.is?(:super)
  	respond_to do |f|
  		if @user.update(user_params)
  			f.html { redirect_to admin_show_user_path(@user), notice: "usuario actualizado!"}
  		else
  			f.html {render :edit}
  		end
  	end
  end

  # DELETE admin user_delete
  def destroy_user
    redirect_to root_path unless current_user.is?(:super)

  	@user.destroy
  	respond_to do |f|
  		f.html { redirect_to admin_path, notice: "usuario removido!"}
  	end
  end

  # GET
  def new_user
    redirect_to root_path unless current_user.is?(:super)

  	@body_class = 'admin'
  	@user = User.new
  end

  # POST
  def create_user
  	@user = User.new(user_params2)
    respond_to do |f|
      if @user.save
        f.html {redirect_to a_users_path, notice: "usuario admin creado!"}
      else
        f.html {redirect_to new_user_path, notice: "algo salio mal!"}
      end
    end
  end


  private
    def check_admin
      unless current_user.is?(:super) or current_user.is?(:admin)
        redirect_to root_path, notice: "?"
      end
    end
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:role, :name)

    end

    def user_params2
      params.require(:user).permit(:role, :name, :email, :password, :password_confirmation)
    end


end
