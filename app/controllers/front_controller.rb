class FrontController < ApplicationController
  before_action :set_product, only: [:show_product]
  # skip_before_action :authenticate_user!, only: [:index, :show_product]
  skip_before_action :authenticate_user!

  def index
  	@st = Product.ransack(params[:q])
    # @c = Car.ransack(params[:q])
    @products = @st.result(distinct: true).published.limit(16)
    @featured = Product.featured
    @auto_parts = Product.category_is('refaccion').limit(16)
    @suspensions = Product.category_is('suspension').limit(16)
    @promos = Promo.all
  end

  def search_result
    @body_class = 'catalog'
  	if params[:tag]
      @products = Product.tagged_with(params[:tag]).published.paginate(per_page: POSTS_PER_PAGE, page: params[:page])

  	end

    @st = Product.ransack(params[:q])
    @products = @st.result(distinct: true).car_search.paginate(per_page: POSTS_PER_PAGE, page: params[:page])
  end

  def brand_search
    @body_class = 'catalog'
    
  	if params[:brand]
  		@products = Product.brand_search(params[:brand]).published.paginate(per_page: POSTS_PER_PAGE, page: params[:page])
  	end
  end

  def show_product
  	@body_class = 'show'
  	@line_item = LineItem.new

    # @folder = MEGA.root.folders[1]
    # @file = @folder.files.first
  end


  # category views
  def tires
    # to do
    @products = Product.category_is('llanta').paginate(per_page: POSTS_PER_PAGE, page: params[:page])
    @body_class = 'catalog'
  end
  def auto_parts
    # to do
    @products = Product.category_is('refaccion').paginate(per_page: POSTS_PER_PAGE, page: params[:page])
    @body_class = 'catalog'

  end
  def suspensions
    # to do
    @products = Product.category_is('suspension').paginate(per_page: POSTS_PER_PAGE, page: params[:page])
    @body_class = 'catalog'

  end

  def offers
    @offers = Product.featured
    @body_class = 'catalog'

  end



  private
  	def set_product
  		@product = Product.find(params[:id])
  	end
end
