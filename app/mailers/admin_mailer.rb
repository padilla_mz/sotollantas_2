class AdminMailer < ApplicationMailer
	default from: "Sistema sotollantas <info@sotollantas.com>"
  # default to: "a@kakto.co"
  default to: EMAIL

  def new_user(user)
  	@user = user
  	mail(subject: "Nuevo usuario! #{user.email}")
  end

  def welcome(user)
  	@user = user
    mail(subject: "Bienvenido a sotollantas #{user.email}!", to: user.email)    
  end

  ## tarjeta de credito

  def charge_paid(charge)
  	@charge = charge

    mail(subject: "sotollantas, cargo con tarjeta de crédito", to: charge.user.email)
  	
  end

  def charge_paid_admin(charge)
    @charge = charge
    # copy for the admin
    mail(subject: "sotollantas, se ha hecho una compra con tarjeta de crédito")
  end


  ## oxxo	

  def oxxo_created(charge)
  	@charge = charge
    mail(subject: "sotollantas, cargo por medio de oxxo", to: charge.user.email)
  end

  def oxxo_created_admin(charge)
    @charge = charge
    # copy for the admin
    mail(subject: "sotollantas, se ha generado un cargo por medio de oxxo")
  end

  # def oxxo_pay_received(charge)
  # 	@charge = charge
  #   mail(subject: "kakto, cargo por medio de oxxo pagado", to: charge.user.email)
  		
  # end


  ## Envio

  def shipped(charge)
  	@charge = charge
    # copy for the admin
    # mail(subject: "kakto, has enviado codigo de rastreo", to: "a@kakto.co")

    # for the user
    mail(subject: "sotollantas, producto ha sido enviado", to: charge.user.email, bcc: EMAIL)
  	
  end
end
