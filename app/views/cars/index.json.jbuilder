json.array!(@cars) do |car|
  json.extract! car, :id, :year, :car_brand_id, :model
  json.url car_url(car, format: :json)
end
