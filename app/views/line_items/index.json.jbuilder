json.array!(@line_items) do |line_item|
  json.extract! line_item, :id, :cart_id, :quantity, :price, :post_id, :charge_id, :property
  json.url line_item_url(line_item, format: :json)
end
