json.extract! @product, :id, :user_id, :title, :content, :published, :published_at, :sku, :weight, :price, :cost_price, :featured, :stock, :likes_count, :category, :created_at, :updated_at
