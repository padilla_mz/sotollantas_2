class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable



  validates :name, presence: true
  # validates :email, presence: true
  validates :name, uniqueness: true

  scope :admins, -> {where(role: :admin)}
  scope :normal_users, -> {where(role: :default)}

  has_many :products

  def likes?(product)
    product.likes.where(user_id: id).any?
  end

  def is?(requested_role)
    self.role == requested_role.to_s
  end

	# def to_param
 #    "#{id}-#{name.parameterize}"
	# end

	after_create :send_notification

  def send_notification
    AdminMailer.new_user(self).deliver
    AdminMailer.welcome(self).deliver
  end

 def do_deposit_transaction(type, token)
   amount = Transaction.amount_for_type(type)
   coupon = UserCoupon.coupon_for_amount(amount)

   card = save_credit_card(token)

   if deposited = deposit(amount, card)
     subscribe if type == 'subscription'
     create_coupon(coupon) if coupon

     deposited
   end
 end


 def deposit(amount, card)
   customer = stripe_customer

   Stripe::Charge.create(
      amount: amount,
      currency: 'mxn',
      customer: customer.id,
      card: card.id,
      description: "Charge for "

    )

  customer.account_balance += amount
  customer.save
 rescue => e
  false
 end


end
