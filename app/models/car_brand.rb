class CarBrand < ActiveRecord::Base
	has_many :cars

	validates :name, uniqueness: true, presence: true
  scope :default, -> {order('name desc')}

end
