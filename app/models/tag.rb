class Tag < ActiveRecord::Base
	validates :name, uniqueness: true, presence: true
	has_many :taggings
	has_many :products, through: :taggings
  scope :default, -> {order('created_at desc')}
	
end
