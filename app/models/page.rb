class Page < ActiveRecord::Base
	validates :title, :content, :published_at, presence: true
end
