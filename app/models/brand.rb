class Brand < ActiveRecord::Base
	has_many :products
	# has_many :brand_models

	validates :name, uniqueness: true, presence: true

  scope :default, -> {order('created_at desc')}

end
