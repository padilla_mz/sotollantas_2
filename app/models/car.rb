class Car < ActiveRecord::Base
  belongs_to :car_brand
  has_many :carings
  has_many :products, through: :carings

  scope :models, -> {select(:model).distinct} 
  scope :default, -> {order('created_at desc')}
  

end
