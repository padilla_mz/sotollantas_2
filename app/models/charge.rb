class Charge < ActiveRecord::Base
  belongs_to :user
	has_many :line_items, dependent: :destroy
	validates :name, :full_address, :phone_number, :country, :state, :city, :zip_code, :colonia, presence: true
  
  scope :default, -> {order('created_at desc')}

	Stages = [['En proceso', :en_proceso], ['Pago recibido y listo para envio', :listo_para_envio], ['Enviado', :enviado]]

  States = [
  	['Aguascalientes'], 
  	['Baja California'], 
  	['Baja California Sur'], 
  	['Campeche'], 
  	['Coahuila'], 
  	['Colima']
  ]


  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
		  item.cart_id = nil
		  line_items << item 
	  end
  end
end
