class LineItem < ActiveRecord::Base
  belongs_to :cart
  belongs_to :product
  belongs_to :charge

  def total_price_line
  	price * quantity
  end



end
