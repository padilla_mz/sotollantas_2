class Cart < ActiveRecord::Base
	has_many :line_items, dependent: :destroy

	def add_product(product_id, product_price, quantity, weight, property='default')
		current_item = line_items.where(product_id: product_id).where(property: property).first
		quantity = quantity.to_i
		weight = weight.to_i.ceil

		if current_item
			current_item.quantity += quantity
			current_item.weight += (weight * quantity)
		else
			current_item = LineItem.new(product_id: product_id, price: product_price, quantity: quantity, property: property, weight: (weight*quantity))
			line_items << current_item
		end
		current_item

	end
	def update_product_quantity(product_id, quantity)
		item = line_items.where(product_id: product_id).first
		weight = item.product.weight.to_i
		quantity = quantity.to_i

		if item
			item.quantity = quantity
			item.weight = (weight * quantity)
		end
		item
	end

	def add_pp(product_id)
		current_item = line_items.find(product_id)
		if current_item
			current_item.quantity += 1
			current_item.weight += current_item.weight
		end
		current_item
	end

	def decrease(product_id, property='default', property_2='default')
		current_item = line_items.find(product_id)
		# current_item = line_items.where(post_id: post_id).where(property: property).where(property_2: property_2).first

		if current_item.quantity > 1
			current_item.quantity -= 1
		else
			current_item.destroy
		end
		current_item
	end

	def total_price_cart
		line_items.to_a.sum { |item| item.total_price_line }
	end
end
