class Product < ActiveRecord::Base
  belongs_to :user
  belongs_to :category
  belongs_to :brand

  has_many :taggings
  has_many :tags, through: :taggings

  has_many :carings
  has_many :cars, through: :carings

  has_many :galleries, dependent: :destroy
  accepts_nested_attributes_for :galleries

  has_many :likes, dependent: :destroy

  has_many :line_items
  has_many :charges, through: :line_items
  before_destroy :ensure_not_referenced_by_any_line_item


  validates :title, length: { maximum: 50, too_long: "%{count} letras son las maximas permitidas!" }, uniqueness: true, presence: true
  validates :sku, length: { in: 7..15, too_long: "%{count} caracteres son las maximas permitidas!", too_short: "%{count} caracteres son los minimos permitidos " }, uniqueness: true, presence: true
  validates :price, numericality: {greater_than_or_equal_to: 2.00}, presence: true
  validates :cost_price, numericality: {greater_than_or_equal_to: 1.00}, presence: true
  validates :content, length: { maximum: 450, too_long: "%{count} letras son las maximas permitidas!" }, presence: true

  validates :brand_id, :ancho, :perfil, :rin, :weight, :stock, :category, presence: true

  scope :default_admin, -> {order('created_at desc')}
  scope :default, -> {order('published_at desc').where("published_at <= ?", DateTime.now)}

  scope :published, -> {default.includes(:tags).where(published: true)}
  scope :featured, -> {published.where(featured: true).limit(16)}

  scope :recent, -> {published.limit(8)}
  scope :car_search, -> {default.includes(:cars).where(published: true)}
  # scope :popular, -> {published.where('likes_count > 5').order('likes_count desc').limit(9)}


  def self.tagged_with(name)
    Tag.find_by_name!(name).products
  end

  def self.brand_search(name)
  	Brand.find_by_name!(name).products
  end

  def self.category_is(name)
    name = name.downcase
    Category.find_by_name!(name).products.published
  end


  def self.tag_counts
    #returns the most used tags

    Tag.select("tags.id, tags.name, count(taggings.tag_id) as count").joins(:taggings).group("taggings.tag_id, tags.id, tags.name").order("count desc").limit(8)
  end


  def tag_list
    #esto retorna un string
    tags.collect(&:name).join(", ")
  end

  def tag_list=(names)
    # self.tags = names.downcase.split(",").map do |n|
    #   Tag.where(name: n.strip).first_or_create!
    # end
    # names.collect do |n|
    #   self.tags << n
    # end

    # names.each do |t|
    #   if t = Tag.find_by_name(name: t)
    #     self.tags << t
    #   end
    # end

    # self.tags << names

    # self.tag_ids = Tag.where(name: names).pluck(:id)

    # names.map {|name| self.tags.find_or_initialize_by(name: name)}
  end

  def to_param
    "#{id}-#{title.parameterize}"
  end

  private
    #ensure that there are no line items referencing this product
    def ensure_not_referenced_by_any_line_item
      if line_items.empty?
        return true
      else
        errors.add(:base, 'Line Items present')
        return false
      end
    end

end
