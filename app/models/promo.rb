class Promo < ActiveRecord::Base

	has_attached_file :img, styles: {
  	large: '1300x500'
  }

  validates_attachment_content_type :img, :content_type => /\Aimage\/.*\Z/
  validates_attachment_file_name :img, :matches => [/png\Z/, /jpe?g\Z/]

  validates :img, :attachment_presence => true
  validates_with AttachmentSizeValidator, :attributes => :img, :less_than => 1.megabytes
  validates :title, presence: true

  Positions = [:slide, :side]

  scope :default, -> {order('title asc')}
  scope :slide_ad, -> {where(position: :slide)}
  scope :side_ad, -> {where(position: :side)}


end
