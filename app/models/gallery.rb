class Gallery < ActiveRecord::Base
	belongs_to :product

	has_attached_file :photo, styles: {
		small: '300x300',
		large: '500x500'
	}
	validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/
	validates :photo, :attachment_presence => true
	validates_attachment_size :photo, :less_than => 1.megabytes
end
