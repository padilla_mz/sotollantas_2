# Sotollantas #

App de ecommerce hecha en Ruby on Rails para el sitio http://www.sotollantas.com/

### Herramientas o elementos construidos ###

* Integración de pagos con en app con la plataforma **[conekta](https://www.conekta.io/es)**
* Panel de control para gestionar: 
    * Usuarios
    * Productos
    * Marcas
    * Categorías
    * Páginas estáticas
    * Slides
* Búsqueda y filtros avanzados con ransack


### Gemas de terceros utilizadas ###
* gem 'devise'
* gem 'cancancan', '~> 1.9'
* gem 'will_paginate', '~> 3.0'
* gem 'ransack'
* gem 'local_time'
* gem 'conekta'
* gem "paperclip", "~> 4.2"