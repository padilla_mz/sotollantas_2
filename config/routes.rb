Rails.application.routes.draw do
  # resources :pages
  # resources :cars
  # resources :car_brands
  resources :promos
  # resources :charges
  # resources :carts
  # resources :years
  resources :line_items do 
    collection do
      # edit_multiple: :post 
      # update_multiple: :put 
      # post 'edit-multiple', to: 'line_items#edit_multiple', as: :edit_multiple
      put 'update-multiple', to: 'line_items#update_multiple', as: :update_multiple
    end
  end

  get 'page/:id', to: 'pages#front_show', as: :front_show
  # resources :products
  # put 'line_items/:id/increase', to: 'line_items#increase', as: :increase_line_item
  # put 'line_items/:id/decrease', to: 'line_items#decrease', as: :decrease_line_item

  # delete 'line_items/:id/destroy', to: 'line_items#destroy', as: :destroy_line_item

  # put 'cart-resume/update-all', to: 'line_items#update_all', as: :update_line_items
  # put 'cart-resume/update-all', to: 'line_items#update_all', as: :update_line_items

  scope '/admin' do
    resources :categories, :tags, :products, :years, :brands, :carts, :cars, :car_brands, :pages

    resources :charges do 
      put 'send-number', to: 'charges#send_number'
    end

    delete 'galleries/:id', to: 'galleries#destroy', as: :delete_gallerie

  end


  # get 'front/index'

  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  # front controller ****************
  root 'front#index'
  get 'products/:id', to: 'front#show_product', as: :show_product
  get 'search', to: 'front#search_result', as: :search_result

  post 'products/:product_id/like', to: 'products/likes#create', as: :increase_like
  delete 'products/:product_id/like', to: 'products/likes#destroy', as: :destroy_like


  get 'cart-resume', to: 'charges#cart_resume', as: :cart_resume
  post 'charge/new', to: 'charges#credit', as: :credit_pay

  get 'charge/new-credit-direct', to: 'charges#credit_1', as: :credit_1
  get 'charge/new-credit-direct-3', to: 'charges#credit_2', as: :credit_2
  get 'charge/new-credit-direct-6', to: 'charges#credit_3', as: :credit_3

  get 'charge/new-oxxo', to: 'charges#oxxo', as: :oxxo
  post 'charge/new-oxxo/pay', to: 'charges#oxxo_pay', as: :oxxo_pay

  get 'charge/show/:id', to: 'charges#show_charge', as: :show_charge

  post 'charge/new-1', to: 'charges#create_1', as: :charge_1
  post 'charge/new-2', to: 'charges#create_2', as: :charge_2
  post 'charge/new-3', to: 'charges#create_3', as: :charge_3


  get "admin", to: 'charges#index'

  # get "admin/charges" => 'charges#index'
  # get "admin/statics" => 'statics#index'
  # get "admin/promos" => 'promos#index'
  get "admin/users/defaults-users" => 'admin#n_users', as: :n_users
  get "admin/users/admins-users" => 'admin#a_users', as: :a_users

  ## Admin user profile
  get "admin/user/new" => 'admin#new_user', as: :new_user
  post "admin/user" => 'admin#create_user'

  get "admin/user/:id" => 'admin#show_user', as: :admin_show_user

  # to update role
  get "admin/user/:id/edit" => 'admin#edit', as: :user_edit
  delete "admin/user/:id" => 'admin#destroy_user', as: :user_delete

  patch "admin/user/:id" => 'admin#update'
  put "admin/user/:id" => 'admin#update', as: :user_edit_s

  get 'tags/:tag', to: 'front#search_result', as: :tag_search
  get 'brand/:brand', to: 'front#brand_search', as: :brand_search

  get 'tires', to: 'front#tires', as: :tires
  get 'auto-parts', to: 'front#auto_parts', as: :auto_parts
  get 'suspensions', to: 'front#suspensions', as: :suspensions
  get 'offers', to: 'front#offers', as: :offers


  # new admin_user



  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
