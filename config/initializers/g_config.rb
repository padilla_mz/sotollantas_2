

SITE_NAME = "sotollantas"
SITE_TITLE = "sotollantas"
SITE_URL = "http://sotollantas.com/"
META_DESCRIPTION = "Sitio sotollantas"
META_KEYWORDS = ""
EMAIL = "sotogoodyear@hotmail.com"
# EMAIL2 = "contacto@holayadira.com"

#will_paginate
POSTS_PER_PAGE = 30

#Locale
TWITTER_LOCALE = "en" #default "en"
FACEBOOK_LOCALE = "en_US" #default "en_US"
GOOGLE_LOCALE =  "es-419" #default "en", espanol latinoamerica "es-419"

#ANALYTICS
GOOGLE_ANALYTICS_ID = "" #your tracking id

#SOCIAL

DISQUS_SHORTNAME = "sotollantas"
# TWITTER_USERNAME1 = "@holaarandas"
FACEBOOK_URL = "https://www.facebook.com/SOTOLLANTAS?fref=ts"
FACEBOOK_NAME = "sotollantas"
INSTAGRAM_URL = "https://instagram.com/sotollantas"
INSTAGRAM_NAME = "sotollantas"
SKYPE_NAME = "sotollantas"
# YOUTUBE = "https://www.youtube.com/channel/UCL61a9rA7ey_V460Yd7RYqg"

# GOOGLE_PLUS_URL = ""

SHIP_PRICE = 140.0
KG_PACKAGE = 40.0

SHIP_COMPANY = "Estafeta"
TRACK_URL = "http://www.estafeta.com/herramientas/rastreo.aspx"


# mega access

# MEGA = Rmega.login('angelpadillam@gmail.com', '19891001aaA')
