# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


admin = User.create(
	name: "Angel Padilla", 
	email: "angelpadillam@gmail.com", 
	role: "super", 
	password: "19891001aaa", 
	password_confirmation: "19891001aaa",
	confirmation_sent_at: "2015-08-11 03:47:15",
	confirmed_at: "2015-08-11 03:51:41",
	)

Category.create(name: 'llanta')
Category.create(name: 'refaccion')
Category.create(name: 'suspension')

# 100.times do |i|
# 	User.create(name: "jorge #{i}", role: "default", email: "aosidj#{i}@hotmail.com", password: "19891001aaa", password_confirmation: "19891001aaa")
# end

# 30.times do |i|
# 	User.create(name: "pepenatas #{i}", role: "admin", email: "juanjuan#{i}7@hotmail.com", password: "19891001aaa", password_confirmation: "19891001aaa")
# end


# 100.times do |i|
# 	Product.create(
# 			title: "llanta 00#{i}",
# 			content: "asodijasd aosidjaoisdjaosidj #{i}",
# 			published: true,
# 			sku: "#{i}0987uyghj",
# 			weight: 2.5,
# 			price: 50,
# 			cost_price: 30,
# 			stock: 100,
# 			ancho: "170",
# 			perfil: "80",
# 			rin: "16",
# 			tire_model_brand: "rr09#{i}",
# 			performance_km: "100000",
# 			category_id: 1,
# 			user_id: 210

# 		)
# end

