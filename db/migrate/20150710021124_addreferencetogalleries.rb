class Addreferencetogalleries < ActiveRecord::Migration
  def change
  	add_reference :galleries, :product, index: true
  end
end
