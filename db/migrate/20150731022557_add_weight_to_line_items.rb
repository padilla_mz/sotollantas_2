class AddWeightToLineItems < ActiveRecord::Migration
  def change
  	add_column :line_items, :weight, :decimal, precision: 8, scale: 3, default: 0.0
  end
end
