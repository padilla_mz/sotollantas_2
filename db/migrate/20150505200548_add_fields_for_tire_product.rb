class AddFieldsForTireProduct < ActiveRecord::Migration
  def change
  	add_column :products, :ancho, :string
  	add_column :products, :perfil, :string
  	add_column :products, :rin, :string

  	add_column :products, :tire_brand, :string
  	add_column :products, :tire_model_brand, :string
  	add_column :products, :performance_km, :string

  	add_column :products, :performance_cold, :integer, default: 5
  	add_column :products, :performance_dry, :integer, default: 5
  	add_column :products, :performance_wet, :integer, default: 5
  end
end
