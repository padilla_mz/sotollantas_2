class ChangesToProducts < ActiveRecord::Migration
  def change
  	change_column :products, :ancho, :integer
  	change_column :products, :perfil, :integer
  	change_column :products, :rin, :integer
  	change_column :products, :performance_km, :integer
  end
end
