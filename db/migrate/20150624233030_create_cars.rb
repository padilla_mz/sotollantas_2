class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.integer :year
      t.references :car_brand, index: true, foreign_key: true
      t.string :model

      t.timestamps null: false
    end
  end
end
