class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.references :user, index: true, foreign_key: true
      t.string :title
      t.text :content
      t.boolean :published, default: true
      t.datetime :published_at
      t.string :sku
      t.decimal :weight, precision: 8, scale: 3, default: 0.0
      t.decimal :price, precision: 8, scale: 2, default: 0.0
      t.decimal :cost_price, precision: 8, scale: 2, default: 0.0
      t.boolean :featured, default: false
      t.integer :stock, default: 0
      t.integer :likes_count, default: 0

      t.timestamps null: false
    end
  end
end
