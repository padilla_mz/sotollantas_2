class ModifyUsers < ActiveRecord::Migration
  def change
  	add_column :users, :name, :string
  	add_column :users, :role, :string, default: "default"
  end
end
