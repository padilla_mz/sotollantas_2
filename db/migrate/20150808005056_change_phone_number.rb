class ChangePhoneNumber < ActiveRecord::Migration
  def change
  	change_column :charges, :phone_number, :integer, limit: 8
  end
end
