class CreateYearings < ActiveRecord::Migration
  def change
    create_table :yearings do |t|
    	t.references :year, index: true, foreign_key: true
    	t.references :product, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
