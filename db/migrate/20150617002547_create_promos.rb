class CreatePromos < ActiveRecord::Migration
  def change
    create_table :promos do |t|
      t.string :title
      t.text :desc
      t.string :url
      t.integer :position

      t.timestamps null: false

    end
    add_attachment :promos, :img
  end
end
