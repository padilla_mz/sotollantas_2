class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
    	t.string :name
      t.timestamps null: false
    end
    remove_column :products, :tire_brand
    add_reference :products, :brand, index: true, foreign_key: true 
  end
end
